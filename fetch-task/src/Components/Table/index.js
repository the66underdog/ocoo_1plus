import React from 'react';
import './style.css';
import { useSelector } from 'react-redux';
import TableRow from '../TableRow';

export default function Table() {

    const rows = useSelector(state => state.vals.values)

    return (
        <table>
            <thead>
                <tr>
                    <th>id</th>
                    <th>symbol</th>
                    <th>name</th>
                </tr>
            </thead>
            <tbody>
                {rows.map((item, index) => (<TableRow key={item.id} row={item} />))}
            </tbody>
        </table>
    );
}