import { createAction, createReducer } from "@reduxjs/toolkit";

const initState = {
    values: []
};

export const setValues = createAction('SET_VALUES');

export default createReducer(initState, {
    [setValues]: (_, action) => {
        return {
            values: action.payload,
        };
    },
});