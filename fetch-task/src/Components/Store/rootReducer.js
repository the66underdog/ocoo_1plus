import { combineReducers, configureStore } from '@reduxjs/toolkit';
import valueReducer from './Reducers/valueReducer.js';

const rootReducer = combineReducers({
    vals: valueReducer,
})

export default configureStore({
    reducer: rootReducer,
})