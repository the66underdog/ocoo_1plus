import React from 'react';

export default function TableRow({ row }) {

    return (
        <tr style={{
            backgroundColor: row.symbol === 'usdt' && 'rgb(77, 136, 84)',
            color: row.symbol === 'usdt' && 'rgba(232, 232, 232, 0.88)'
        }}>
            <td>{row.id}</td>
            <td>{row.symbol}</td>
            <td>{row.name}</td>
        </tr>
    );
}