import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import './App.css';
import { setValues } from './Components/Store/Reducers/valueReducer';
import Table from './Components/Table';

function App() {

  const dispatch = useDispatch();

  useEffect(() => {
    const controller = new AbortController();
    async function fetchValues() {
      try {
        const res = await fetch('https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=250&page=1', {
          signal: controller.signal,
        });
        if (res.ok) {
          const result = await res.json();
          dispatch(setValues(result));
          console.log(result);
        }
      }
      catch (err) {
        console.log(err);
      }
    }
    fetchValues();
    return () => { controller.abort() };
  })

  return (
    <div className="app">
      <Table />
    </div>
  );
}

export default App;
